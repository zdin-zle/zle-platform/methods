/* Any JavaScript here will be loaded for all users on every page load. */


document.getElementById("p-tb-label").addEventListener("click", function() {
    const dropdown = document.querySelector(".portal .body");
    const footer = document.querySelector(".footer_top_box");
    
    if (dropdown.style.display === "block") {
        // Dropdown is displayed, increase footer margin
        footer.style.marginTop = dropdown.scrollHeight + 20+ "px";
    } else {
        // Dropdown is hidden, reset footer margin
        footer.style.marginTop = "0";
    }
});

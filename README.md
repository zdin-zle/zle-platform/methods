# **METHODS component for the ZLE Platform**
---
This repository contians the files for the installation of the required packages for the stablishment of a Knowledge Management Plattform for the Zukunfts Labor Energie

At the moment this platform is available for developers only  [`https://dev-zle.offis.de/methods/`](https://dev-zle.offis.de/methods/)



## First Time Installation
---
The files of this repository must be cloned to a repository. At the moment it is in the ZLE Servers VM4 under
```
/home/dev/methods/
```
In this folder there must be also a `.env` file that contins the environment variables required for the installation
```
/home/dev/methods/.env
```
This repository must be cloned in this folder using:
```
git clone git@gitlab.com:zdin-zle/zle-platform/methods.git
```
In the docker compose file;
```
/home/dev/methods/methods/zle-mediawiki/docker-compose.yml
```
The following lines must be commented for a first time installation:
```
 - ./data/conf/LocalSettings.php:/var/www/html/methods/LocalSettings.php:ro

 - ./data/skins/MinervaNeue:/var/www/html/methods/skins/MinervaNeue:ro
 - ./data/skins/Metrolook:/var/www/html/methods/skins/Metrolook:ro
```

This can be done in the gitlab repository, and then the local copy must be updated:
```
sudo git pull
```

Go to the folder with the 'docker-compose.yml' (/methods/zle-mediawiki/) and create the containters

```
sudo docker compose --env-file /home/dev/methods/.env  up -d
```

This will allow setting up the wikimedia for the first time.

In the during the installation, the fields must be filled with the information in the `.env` variables in the main methods folder

```
Site Name: ZLE Methods
Database Type: mysql
DB Server: methods_mariadb
DB Name: (see environment variables)
DB User:  (see environment variables)
DB Password: (see environment variables)
DB Prefix: zle

```
For the first time logggin in, the following variables must be given

```
ADMIN USERNAME: (see environment variables)
ADMIN PASSWORD: (see environment variables)

```

Afther that, the `LocalSettings.php` file must be downloaded.
Following variables must be taken from the `LocalSettings.php` and saved (or replaced) MANUALLY in the `.env` file:
```
----Wikimedia Keys
----$wgSecretKey  
SECRET_KEY=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

----$wgUpgradeKey  
UPGRADE_KEY =xxxxxxxxxxxxxxxx
```
After that, the current version of the `LocalSettings.php` can be used since it has already the variables as environment variables.

For this, the sections in the docker container file must be uncommented in the repository (can be done directly in gitlab and commited):

```
 - ./data/conf/LocalSettings.php:/var/www/html/methods/LocalSettings.php:ro


 - ./data/skins/MinervaNeue:/var/www/html/methods/skins/MinervaNeue:ro
 - ./data/skins/Metrolook:/var/www/html/methods/skins/Metrolook:ro
```

The containers must be first paused:

```
sudo docker container stop methods_mariadb; sudo docker container stop methods_mediawiki;sudo docker container stop methods_memcached
```

The repository must be updated to include the uncommenting of the `docker-container.yml` file:
```
sudo git pull
```
And then the containers can be built once again by going to the folder with the `docker-container.yml` file and building the containers with:

```
sudo docker compose --env-file /home/dev/methods/.env  up -d
```

## Modifications to the LocalSettings.php
---

In case of a new `LocalSettings.php` datafile, several modifications are required to work with the ZLE website:
```
$wgScriptPath = "/methods";
$wgServer = "https://dev-zle.offis.de";

$wgLogos = [
	'1x' => "$wgResourceBasePath/images/logos/zle.svg",
	'icon' => "$wgResourceBasePath/images/logos/zle.svg",
];

$wgDBname = getenv('DB_NAME');
$wgDBuser = getenv('DB_USER');
$wgDBpassword = getenv('DB_PASS');
$wgEnableUploads = true;


$wgSecretKey =getenv('SECRET_KEY');
$wgUpgradeKey =getenv('UPGRADE_KEY');

$wgDefaultSkin = "metrolook";
wfLoadSkin( 'Metrolook' );

$wgShowExceptionDetails = true;
$wgDevelopmentWarnings = true; error_reporting( -1 ); ini_set( 'display_errors', 1);
$wgAllowSiteCSSOnRestrictedPages = true; 
```

## Setting the Wiki Style
---

By default the CSS style is the one on the 'Metrolook' template.

To modify the style according to the ZLE Corporate Design, the contents of the file
```
/zle-mediawiki/data/extra_content/common.css
```

Have to be copied and pasted in the View source for MediaWiki:Common.css

```
METHODS_DOMAIN/index.php?title=MediaWiki:Common.css&action=edit
```

Further modifications to the Wiki style can be made directly on the editable website, but it is STRONGLY recommended to save this on the `comon.css` of the git, otherwise modification will be lost when the container is removed


## Adding additional functionality to the Wiki
---

If new features need to be added, they can be incorporated in the Common.js. 

To expand the funcionts, the the contents of the file
```
/zle-mediawiki/data/extra_content/common.js
```

Have to be copied and pasted in the View source for MediaWiki:Common.js

```
METHODS_DOMAIN/index.php?title=MediaWiki:Common.js&action=edit
```

Further modifications to the Wiki functions can be made directly on the editable website, but it is STRONGLY recommended to save this on the `comon.js` of the git, otherwise modification will be lost when the container is removed.



## Add your files
---


- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/zdin-zle/zle-platform/methods.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools
---


- [ ] [Set up project integrations](https://gitlab.com/zdin-zle/zle-platform/methods/-/settings/integrations)

## Collaborate with your team
---


- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

